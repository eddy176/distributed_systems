package main

import (
	"fmt"
	"log"
	"path/filepath"
)

// MapTask export
type MapTask struct {
	M, R       int    // total number of map and reduce tasks
	N          int    // map task number, 0-based
	SourceHost string // address of host with map input file
}

// ReduceTask export
type ReduceTask struct {
	M, R        int      // total number of map and reduce tasks
	N           int      // reduce task number, 0-based
	SourceHosts []string // addresses of map workers
}

// Pair export
type Pair struct {
	Key   string
	Value string
}

// Interface export
type Interface interface {
	Map(key, value string, output chan<- Pair) error
	Reduce(key string, values <-chan string, output chan<- Pair) error
}

//for applying consistent naming to avoid typos for the master
func mapSourceFile(m int) string       { return fmt.Sprintf("map_%d_source.db", m) }
func mapInputFile(m int) string        { return fmt.Sprintf("map_%d_input.db", m) }
func mapOutputFile(m, r int) string    { return fmt.Sprintf("map_%d_output_%d.db", m, r) }
func reduceInputFile(r int) string     { return fmt.Sprintf("reduce_%d_input.db", r) }
func reduceOutputFile(r int) string    { return fmt.Sprintf("reduce_%d_output.db", r) }
func reducePartialFile(r int) string   { return fmt.Sprintf("reduce_%d_partial.db", r) }
func reduceTempFile(r int) string      { return fmt.Sprintf("reduce_%d_temp.db", r) }
func makeURL(host, file string) string { return fmt.Sprintf("http://%s/data/%s", host, file) }

// Process Maptask
func (task *MapTask) Process(tempdir string, client Interface) error {
	// join

	fmt.Println("MapTask", "tempdir:", tempdir, "interface", client, "task:", task)

	outputpath := filepath.Join(tempdir, mapInputFile(task.N))
	fmt.Println("output path", outputpath)
	fmt.Println("task.sourcehost", task.SourceHost, "outputpath", outputpath)
	// download input file
	fmt.Println("here", task.SourceHost, outputpath)
	err := download(task.SourceHost, outputpath)
	if err != nil {
		log.Fatalf("maptask download error %s:", err)
	}
	/*
		// list of databases
		reduceDB := make([]*sql.DB, 0)
		for i := 0; i < task.R; i++ {
			//get output of joined filename
			output := filepath.Join(tempdir, mapOutputFile(task.N, i))
			db, err := createDatabase(output)
			if err != nil {
				log.Fatalf("maptask creating outputdb error %s:", err)
			}
			defer db.Close()
			//append db to reducedb list
			reduceDB = append(reduceDB, db)
		}

		input, err := openDatabase(outputpath)
		if err != nil {
			log.Fatalf("maptask opening inputdb error %s:", err)
		}
		defer input.Close()
		rows, err := input.Query("select key, value from pairs;")
		if err != nil {
			log.Fatalf("maptask input query error %s:", err)
		}
		defer rows.Close()

		//select which output file a given pair should go to, use a hash of the output key:
		outputfunc := func(output chan Pair, ack chan bool) {
			for pair := range output {
				hash := fnv.New32()
				hash.Write([]byte(pair.Key))
				r := int(hash.Sum32()) % task.R
				_, err := reduceDB[r].Exec(
					"INSERT INTO pairs (key, value) VALUES (?, ?);",
					pair.Key,
					pair.Value)
				if err != nil {
					log.Fatalf("maptask inserting error %s:", err)
				}
			}
			ack <- true
		}

		// dbquery to select all pairs from source file
		for rows.Next() {
			Pairchan := make(chan Pair, 100)
			ackchan := make(chan bool, 1)

			//get keys and values
			var key, value string
			var p Pair
			err := rows.Scan(&key, &value)
			if err != nil {
				return err
			}

			p.Key = key
			p.Value = value

			if err != nil {
				log.Fatalf("maptask rows error %s:", err)
			}
			//call map on pair
			//replace key and value with pair.key and pair.value?
			go client.Map(p.Key, p.Value, Pairchan)
			if err != nil {
				log.Fatalf("maptask rows error %s:", err)
			}

			go outputfunc(Pairchan, ackchan)
			<-ackchan
		}
	*/
	return nil

}

// Process ReduceTask
func (task *ReduceTask) Process(tempdir string, client Interface) error {
	//merge all appropriate output dbs from map phase
	input := filepath.Join(tempdir, reduceInputFile(task.N))
	log.Printf("This is input %v\n", input)
	//create input db
	tempFile := reduceTempFile(task.N)
	//merge the input and temp
	source, err := mergeDatabases(task.SourceHosts, input, tempFile)
	if err != nil {
		log.Fatalf("reducetask merge error %s:", err)
	}

	defer source.Close()
	log.Println("merged")
	//reduce
	outputFile := reduceOutputFile(task.N)
	//get the output path
	outputPath := filepath.Join(tempdir, outputFile)

	//output database
	outputdb, err := createDatabase(outputPath)
	if err != nil {
		log.Fatalf("reducetask create outputdb error %s:", err)
	}

	defer outputdb.Close()

	//process all pairs in correct order
	rows, err := source.Query(`select key, value from pairs order by key, value`)
	if err != nil {
		log.Fatalf("reducetask query error %s:", err)
	}

	defer rows.Close()

	//set an empty string to test current vs past
	tempkey := ""
	//client channel
	inchan := make(chan string, 100)
	//server channel
	outchan := make(chan Pair, 100)
	//boolean channel
	boolchan := make(chan bool)

	// make handlePairs
	handlePairs := func(values chan Pair) {
		// for every pair, insert it into the db
		for pair := range values {
			_, err := outputdb.Exec(
				"INSERT INTO pairs (key, value) VALUES (?, ?);",
				pair.Key,
				pair.Value)
			if err != nil {
				log.Fatalf("reducetask inserting error %s:", err)
			}
		}
		// return true to the channel
		boolchan <- true
	}

	for rows.Next() {
		//get the pair
		var key, value string
		var p Pair
		err := rows.Scan(&key, &value)
		if err != nil {
			return err
		}
		p.Key = key
		p.Value = value
		if err != nil {
			log.Fatalf("reducetask rows error %s:", err)
		}

		//gives temp key to check is our key has changed
		if tempkey == "" {
			//save current key
			tempkey = p.Key
			//go reduce!!
			go client.Reduce(tempkey, inchan, outchan)
			//go handle pairs!!
			go handlePairs(outchan)
		}
		//when we get a new tempkey check
		if tempkey != p.Key {
			//close previous call
			//close channel
			close(inchan)
			<-boolchan
			//new call to client.reduce
			inchan = make(chan string, 100)
			outchan = make(chan Pair, 100)
			tempkey = p.Key
			go client.Reduce(tempkey, inchan, outchan)
			go handlePairs(outchan)
		}
		inchan <- p.Value
	}
	//close channel
	close(inchan)
	//wait to finish
	<-boolchan
	return nil
}
