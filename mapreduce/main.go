package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"unicode"
)

// globals
const (
	M           = 9
	R           = 3
	port        = ":8080"
	tempdir     = "/tmp/data/"
	address     = "http://localhost"
	downloadDir = "splits/"
)

// Client struct
type Client struct{}

// Map function
func (c Client) Map(key, value string, output chan<- Pair) error {
	defer close(output)
	lst := strings.Fields(value)
	for _, elt := range lst {
		word := strings.Map(func(r rune) rune {
			if unicode.IsLetter(r) || unicode.IsDigit(r) {
				return unicode.ToLower(r)
			}
			return -1
		}, elt)
		if len(word) > 0 {
			output <- Pair{Key: word, Value: "1"}
		}
	}
	return nil
}

// Reduce function
func (c Client) Reduce(key string, values <-chan string, output chan<- Pair) error {
	defer close(output)
	count := 0
	for v := range values {
		i, err := strconv.Atoi(v)
		if err != nil {
			return err
		}
		count += i
	}
	p := Pair{Key: key, Value: strconv.Itoa(count)}
	output <- p
	return nil
}

func main() {
	runtime.GOMAXPROCS(1)
	os.Mkdir(tempdir, 0700)
	//defer os.RemoveAll(tempdir)

	// serve files in tempdir
	go func() {
		http.Handle("/", http.StripPrefix("/", http.FileServer(http.Dir(tempdir))))
		if err := http.ListenAndServe(port, nil); err != nil {
			log.Printf("Error in HTTP server for %s: %v", port, err)
		}
	}()

	// split database into M files
	var urls, err = splitDatabase("austen.db", tempdir, "output-%d.db", M)
	if err != nil {
		log.Fatalf("error splitting %s", err)
	}

	// download from localhost
	for _, filename := range urls {
		fmt.Println("splitting file...")
		download(address+port+"/"+filename, downloadDir+filename)
	}

	// create insert files

	c := Client{}
	fmt.Println("SourceHost:", address+port)
	mapper := MapTask{M: M, R: R, N: 0, SourceHost: address + port}
	mapper.Process(tempdir, c)

	//download("http://localhost:8080/output-0.db", "test.db")
	//mergeDatabases(urls, "merged.db", "http://localhost:8080/")
}
