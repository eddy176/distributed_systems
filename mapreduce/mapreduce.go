package main

import (
	"database/sql"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

//WEEK 1 helper funcs database and network handling code

func openDatabase(path string) (*sql.DB, error) {
	// the path to the database--this could be an absolute path
	options :=
		"?" + "_busy_timeout=10000" +
			"&" + "_case_sensitive_like=OFF" +
			"&" + "_foreign_keys=ON" +
			"&" + "_journal_mode=OFF" +
			"&" + "_locking_mode=NORMAL" +
			"&" + "mode=rw" +
			"&" + "_synchronous=OFF"
	db, err := sql.Open("sqlite3", path+options)
	if err != nil {
		// handle the error here
		log.Fatalf("Error opening database %s", err)
	}

	//fmt.Println(db)

	return db, nil
}

func createDatabase(path string) (*sql.DB, error) {
	os.Remove("/tmp/data" + path)
	db, err := openDatabase(path)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`create table pairs (key text, value text);`)

	return db, nil
}

func splitDatabase(source, outputDir, outputPattern string, m int) ([]string, error) {
	//opendb
	db, err := openDatabase(source)
	if err != nil {
		log.Fatalf("splitting database %s:", err)
	}
	defer db.Close()

	//list of databases
	outputdbs := make([]*sql.DB, 0)
	//list of names
	pathnames := make([]string, 0)
	// m slices
	for i := 0; i < m; i++ {
		pathname := outputDir + mapSourceFile(i)
		//fmt.Println(pathname)
		// get the output name
		//pathname := fmt.Sprintf(outputDir+outputPattern, i)
		// create new database
		outputdb, err := createDatabase(pathname)
		if err != nil {
			log.Fatalf("splitting database %s:", err)
		}
		defer outputdb.Close()
		// append new database to list
		outputdbs = append(outputdbs, outputdb)
		// append new name to list
		pathnames = append(pathnames, mapSourceFile(i))
	}
	// get pairs from db
	rows, err := db.Query("select key, value from pairs;")
	if err != nil {
		log.Fatalf("splitting databases selecting %s:", err)
	}

	defer rows.Close()
	index := 0
	// for each pair
	for rows.Next() {
		// get pair from rows
		var key, value string
		serr := rows.Scan(&key, &value)
		if serr != nil {
			log.Fatalf("getting key-value pairs %s: ", err)
		}
		db := outputdbs[index%m]
		//fmt.Print("db", db)
		_, err := db.Exec(`insert into pairs (key, value) values (?, ?)`, key, value)
		if err != nil {
			log.Fatalf("Error inserting %s ", err)
		}
		index++

	}
	if err := rows.Err(); err != nil {
		log.Fatalf("Error %s:", err)
	}

	return pathnames, nil
}

//mergedb
func mergeDatabases(urls []string, filename string, host string) (*sql.DB, error) {
	outputdb, err := createDatabase("/tmp/data/" + filename)
	if err != nil {
		log.Fatalf("Error creating db %s:", err)
	}

	for _, url := range urls {
		fmt.Println(url)
		derr := download(host+url, url)
		if derr != nil {
			log.Fatalf("Error with download %s:", derr)
		}
		gerr := gatherInto(outputdb, "/tmp/data/"+url)
		if gerr != nil {
			log.Fatalf("Error with Gatherinto %s", gerr)
		}
		rerr := os.Remove("/tmp/data/" + url)
		if rerr != nil {
			log.Fatalf("Error removing %s:", rerr)
		}
	}
	return outputdb, nil
}

//download
func download(url, filename string) error {
	mpath, err := os.Create(filename)
	if err != nil {
		log.Fatalf("Error creating path %s:", err)
	}
	defer mpath.Close()

	//get body from url
	res, err := http.Get(url)
	if err != nil {
		log.Fatalf("Error getting %s:", err)
	}
	defer res.Body.Close()

	//read body into mpath
	_, err = io.Copy(mpath, res.Body)
	if err != nil {
		log.Fatalf("Error reading body %s:", err)
	}

	return nil
}

//gatherinto
func gatherInto(db *sql.DB, path string) error {
	//query := fmt.Sprintf("attach '%s' as merge;", path) // ?
	//fmt.Println("gatherinto", path)
	openDatabase(path)
	_, err := db.Exec(`ATTACH ? AS merge;`, path)
	if err != nil {
		log.Printf("Error executing query: attach %s as merge.\n", path)
		return err
	}

	_, err = db.Exec(`INSERT INTO pairs SELECT * FROM merge.pairs;`)
	if err != nil {
		log.Fatalf("Gatherinto insert %s:", err)
	}

	_, err = db.Exec(`DETACH merge;`)
	if err != nil {
		log.Fatalf("Gatherinto detaching %s:", err)
	}

	return nil
}

func countDbLines(filename string) int {
	db, err := openDatabase(filename)
	if err != nil {
		log.Fatalf("splitting database %s:", err)
	}
	defer db.Close()

	var (
		count int
	)

	rows, err := db.Query("SELECT count(1) FROM pairs AS count", 1)
	if err != nil {
		log.Fatalf("counting database %s:", err)
	}

	for rows.Next() {
		//fmt.Println(rows)
		err := rows.Scan(&count)
		if err != nil {
			log.Fatal(err)
		}
	}

	return count

}

func insertPair(pair Pair, db *sql.DB) error {
	_, err := db.Exec(
		"INSERT INTO pairs (key, value) VALUES (?, ?);",
		pair.Key,
		pair.Value)
	if err != nil {
		log.Fatalf("Inserting pair %s failed", err)
	}
	return nil
}

//test

//WEEK 2 map and reduce handles

//WEEK 3 master and worker node organization
