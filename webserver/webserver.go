package main

import (
	"log"
	"net/http"
)

func main() {
	address := ":8080"
	tempdir := "/tmp/data"

	log.Printf("starting http server at %s", address)

	http.Handle("/", http.StripPrefix("/", http.FileServer(http.Dir(tempdir))))
	if err := http.ListenAndServe(address, nil); err != nil {
		log.Printf("Error in HTTP server for %s: %v", address, err)
	}

}
