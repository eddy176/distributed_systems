package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var commands = make(map[string]func(string))

var directions = map[int]string{
	0: "north",
	1: "east",
	2: "west",
	3: "south",
	4: "up",
	5: "down",
}

func getAllowedCmds() []string {
	// returns a slice of accepted commands
	allCommands := []string{
		"north",
		"east",
		"south",
		"west",
		"look",
		"eat",
		"recall",
	}
	return allCommands
}

func commandLoop() error {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Enter a Command: ")
	for scanner.Scan() {
		line := scanner.Text()
		doCommand(line)
	}
	if err := scanner.Err(); err != nil {
		return fmt.Errorf("in main command loop: %v", err)
	}

	return nil
}

func addCommand(cmd string, f func(string)) {
	for i := range cmd {
		if i == 0 {
			continue
		}
		prefix := cmd[:i]
		commands[prefix] = f
	}
	commands[cmd] = f
}

func doCommand(cmd string) error {

	words := strings.Fields(cmd)
	if len(words) == 0 {
		return nil
	}

	if f, exists := commands[strings.ToLower(words[0])]; exists {
		f(cmd)
		fmt.Print("Enter a Command: \n")
	} else {
		fmt.Print("Command doesn't exist!\n")
		fmt.Print("Enter a Command: \n")
	}
	return nil
}

func lookupCommand(cmd string) int {
	var doorindex int
	//fmt.Printf("here %s", cmd[:1])

	switch cmd[:1] {
	case "n":
		doorindex = 0
	case "e":
		doorindex = 1
	case "w":
		doorindex = 2
	case "s":
		doorindex = 3
	case "u":
		doorindex = 4
	case "d":
		doorindex = 5
	}

	return doorindex
}

func doMove(direction string, lookonly bool) bool {
	index := lookupCommand(direction)
	valid := false
	fmt.Println("Moving From: ", me.Location.ID)
	fmt.Println("Exit ID", me.Location.Exits[index].To.ID)

	if me.Location.Exits[index].Valid {
		me.Location = me.Location.Exits[index].To
		valid = true
	}

	fmt.Println("Moved to: ", me.Location.ID)
	return valid
}

func doLook(input string) {
	words := strings.Fields(input)

	if len(words) == 1 {
		fmt.Printf("%s\n%s\n\nExits:\n", me.Location.Name, me.Location.Description)

		for i, _ := range me.Location.Exits {
			if me.Location.Exits[i].Valid {
				fmt.Printf(
					"%s: %s",
					directions[i],
					me.Location.Exits[i].Description)
			}
		}
	} else {
		// look in direction
		cmdIndex := lookupCommand(words[1])
		fmt.Println("look in direction")

		if me.Location.Exits[cmdIndex].Valid {
			fmt.Print(me.Location.Exits[cmdIndex])
		} else {
			fmt.Println("You cannot go that way")
		}
	}
}
func doRecall(input string) {
	fmt.Println("Moving back to beginning")
	me.Location = me.StartRoom
}

func doEat(input string) {
	fmt.Printf("You ate.\n")
}
func doSmile(s string) {
	fmt.Printf("You smile happily.\n")
}

func doWest(input string) {
	doMove("w", false)
	fmt.Printf("You moved west.\n")
}

func doEast(input string) {
	doMove("e", false)
	fmt.Printf("You moved east.\n")
}

func doSouth(s string) {
	doMove("s", false)
	fmt.Printf("You moved south.\n")
}
func doNorth(input string) {
	doMove("n", false)
	fmt.Printf("You moved north.\n")
}
func doUp(input string) {
	doMove("u", false)
	fmt.Printf("Move up.\n")
}
func doDown(input string) {
	doMove("d", false)
	fmt.Printf("Move down.\n")
}

func initCommands() {
	addCommand("smile", doSmile)
	addCommand("eat", doEat)

	addCommand("l", doLook)
	addCommand("lo", doLook)
	addCommand("loo", doLook)
	addCommand("look", doLook)
	addCommand("w", doWest)
	addCommand("we", doWest)
	addCommand("wes", doWest)
	addCommand("west", doWest)
	addCommand("e", doEast)
	addCommand("ea", doEast)
	addCommand("east", doEast)
	addCommand("s", doSouth)
	addCommand("so", doSouth)
	addCommand("sou", doSouth)
	addCommand("sout", doSouth)
	addCommand("south", doSouth)
	addCommand("n", doNorth)
	addCommand("no", doNorth)
	addCommand("nor", doNorth)
	addCommand("nort", doNorth)
	addCommand("north", doNorth)
	addCommand("d", doDown)
	addCommand("do", doDown)
	addCommand("dow", doDown)
	addCommand("down", doDown)
	addCommand("u", doUp)
	addCommand("up", doUp)
	addCommand("r", doRecall)
	addCommand("re", doRecall)
	addCommand("rec", doRecall)
	addCommand("reca", doRecall)
	addCommand("recal", doRecall)
	addCommand("recall", doRecall)
}
