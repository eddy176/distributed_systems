package main

import (
	"database/sql"
	"fmt"
	"log"
)

//Zone is
type Zone struct {
	ID    int
	Name  string
	Rooms []*Room
}

//Room is
type Room struct {
	ID          int
	Zone        *Zone
	Name        string
	Description string
	Exits       [6]Exit
}

//Exit is
type Exit struct {
	To          *Room
	Description string
	Valid       bool
}

func readZone(tx *sql.Tx) map[int]*Zone {

	rows, err := tx.Query("SELECT id, name FROM zones")
	/*
		if err != nil {
			defer tx.Rollback()
		} else {
			tx.Commit()
		}
	*/

	zones := make(map[int]*Zone)
	for rows.Next() {
		var id int
		var name string
		rows.Scan(&id, &name)
		err = rows.Scan(&id, &name)
		if err != nil {
			log.Fatal(err)
		}

		zone := Zone{}
		zone.ID = id
		zone.Name = name
		p := &zone

		zones[id] = p
	}

	return zones

}

func readRooms(tx *sql.Tx, zones map[int]*Zone) map[int]*Room {
	var rooms = make(map[int]*Room)
	rows, _ := tx.Query("SELECT id, zone_id, name, description FROM rooms;")
	/*if err != nil {
		defer tx.Rollback()
	} else {
		tx.Commit()
	}*/

	for rows.Next() {
		var id int
		var zoneid int
		var name string
		var description string
		rows.Scan(&id, &zoneid, &name, &description)

		room := Room{}
		room.ID = id
		room.Name = name
		room.Zone = zones[zoneid] // pointer to zone from zones map
		room.Description = description

		for i := 0; i <= 5; i++ {
			room.Exits[i].Valid = false
		}
		p := &room

		// set default player room
		if room.ID == 3001 {
			me.Location = p
			me.StartRoom = p
		}

		rooms[id] = p
	}

	return rooms
}

func findRoom(targetID int, roomTo *Room, rooms map[int]*Room) *Room {
	for _, room := range rooms {
		if targetID == room.ID {
			return room
		}
	}
	return nil
}

func assignExits(tx *sql.Tx, rooms map[int]*Room) {
	for id, room := range rooms {
		query := fmt.Sprintf("SELECT from_room_id, to_room_id, direction, description FROM exits WHERE from_room_id=%d;", id)
		rows, _ := tx.Query(query)
		//fmt.Println(id)

		for rows.Next() {
			var from_room_id int
			var to_room_id int
			var direction string
			var description string
			rows.Scan(&from_room_id, &to_room_id, &direction, &description)
			//var r [6]*Exit
			doorindex := 0

			switch direction {
			case "n":
				doorindex = 0
			case "e":
				doorindex = 1
			case "w":
				doorindex = 2
			case "s":
				doorindex = 3
			case "u":
				doorindex = 4
			case "d":
				doorindex = 5
			}

			room.Exits[doorindex].To = findRoom(to_room_id, room.Exits[doorindex].To, rooms)
			room.Exits[doorindex].Description = description
			room.Exits[doorindex].Valid = true

		}
	}
}
