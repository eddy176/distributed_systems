package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

type Player struct {
	Name      string
	Location  *Room
	StartRoom *Room
}

var me = Player{}

func main() {

	log.SetFlags(log.Ltime | log.Lshortfile)
	initCommands()
	me.Name = "P1"
	// the path to the database--this could be an absolute path
	path := "world.db"
	options :=
		"?" + "_busy_timeout=10000" +
			"&" + "_case_sensitive_like=OFF" +
			"&" + "_foreign_keys=ON" +
			"&" + "_journal_mode=WAL" +
			"&" + "_locking_mode=NORMAL" +
			"&" + "mode=rw" +
			"&" + "_synchronous=NORMAL"
	db, err := sql.Open("sqlite3", path+options)
	if err != nil {
		log.Fatal(err)
	}
	tx, _ := db.Begin()
	zones := readZone(tx)
	rooms := readRooms(tx, zones)
	assignExits(tx, rooms)

	if err := commandLoop(); err != nil {
		log.Fatalf("%v", err)
	}

	fmt.Print("Enter a Command: ")
	scanner := bufio.NewScanner(os.Stdin)
	for {

		scanner.Scan()
		text := scanner.Text()
		fmt.Print("Enter a Command: ")

		doCommand(text)

	}
}
