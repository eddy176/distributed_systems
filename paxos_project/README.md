# _Paxos Consensus Algorithm_
   ### Project based on the article written by Leslie Lamport.
    https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/tr-2005-33.pdf
    https://lamport.azurewebsites.net/pubs/lamport-paxos.pdf
