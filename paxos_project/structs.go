package main

import (
	"sync"
)

//CHATTY var for chatty func
var CHATTY = 2

//Nothing struct
type Nothing struct{}

//Node struct
type Node struct {
	Address  string
	Cell     []string
	Slot     []Slot // represents a single command to apply to database
	database map[string][]string
	ToApply  int
	Mutex    sync.Mutex
	Acks     map[string]chan string
	PReplies []chan PResponse
}

//Slot struct
type Slot struct {
	Decided  bool
	Command  Command
	Promise  Seq
	Accepted Command
	HighestN int
}

//Seq sequence struct
type Seq struct {
	Number  int
	Address string
}

//Command struct
type Command struct {
	Command string
	Address string
	Seq     Seq
	Tag     int
}

//Prepare struct
type Prepare struct {
	Slot       Slot
	Sequence   Seq
	SlotNumber int
}

//PResponse prepare response Struct
type PResponse struct {
	Okay    bool
	Slot    Slot
	Promise Seq
	Command Command
}

//Accept struct
type Accept struct {
	Slot     Slot
	Sequence Seq
	Command  Command
	SlotNum  int
}

//AResponse accept response struct
type AResponse struct {
	Okay    bool
	Promise Seq
}

//Decision struct
type Decision struct {
	Slot       Slot
	Command    Command
	SlotNumber int
}

//DResponse decide response struct
type DResponse struct {
	Okay      bool
	Requested string
	Key       string
	Value     []string
	Address   string
	Reply     string
}

//KeyValue struct
type KeyValue struct {
	Key   string
	Value string
}
