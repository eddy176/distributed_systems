package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

//Prepare command
func (n *Node) Prepare(prep Prepare, reply *PResponse) error {
	n.Mutex.Lock()
	tempreply := *reply
	slotToWork := n.Slot[prep.SlotNumber]
	currentSeq := slotToWork.Promise
	proposedSeq := n.Slot[prep.SlotNumber].Promise

	//was this slot decided or did I already promise a higher sequence number?
	chatty(1, fmt.Sprintf("Prepare Current Seq = [%d]\nProposed = [%d]\n", currentSeq.Number, proposedSeq.Number))
	if currentSeq.Cmp(proposedSeq) > 0 {
		chatty(1, fmt.Sprintf("[%d] already promised ---> [%s]", prep.Slot.HighestN, slotToWork.Promise.Address))
		tempreply.Okay = false
		tempreply.Promise = proposedSeq
	} else {
		tempreply.Okay = true
		tempreply.Promise = prep.Slot.Promise
		tempreply.Command = n.Slot[prep.SlotNumber].Command
		chatty(1, fmt.Sprintf("Successful Prepare round: SeqN[%d]", proposedSeq.Number))
		n.Slot[prep.SlotNumber].Promise.Number = prep.Sequence.Number
		n.Slot[prep.SlotNumber].Promise.Address = prep.Sequence.Address
	}
	*reply = tempreply
	n.Mutex.Unlock()
	return nil
}

//Accept command
func (n *Node) Accept(acc Accept, reply *AResponse) error {
	tempreply := *reply
	slot := n.Slot[acc.SlotNum]
	lastLocalSeq := slot.Promise
	commandedSeqNum := acc.Sequence
	n.Mutex.Lock()
	chatty(1, fmt.Sprintf("I have been asked to accept Seq: [%d]\nCompare: [%d]\n", commandedSeqNum.Number, lastLocalSeq.Number))
	println(commandedSeqNum.Cmp(lastLocalSeq))
	if commandedSeqNum.Cmp(lastLocalSeq) > -1 {

		//set the return, since it was successful we do not need to
		//send back the command
		chatty(1, "Can accept\n")
		tempreply.Okay = true
		tempreply.Promise = lastLocalSeq
		n.Slot[acc.SlotNum].Command = acc.Command
	} else {
		//the value was not placed
		//inform the sender and return the
		//command that we have in this slot
		chatty(1, "Can not accept\n")
		tempreply.Okay = false
		tempreply.Promise = lastLocalSeq
	}
	*reply = tempreply
	n.Mutex.Unlock()
	return nil
}

//Decide command
func (n *Node) Decide(dec Decision, reply *DResponse) error {
	n.Mutex.Lock()
	tempreply := *reply
	chatty(1, fmt.Sprintf("[%v] Decide: called with Command={%v}\n", n.ToApply, dec.Command))

	if n.Slot[dec.SlotNumber].Decided && !n.Slot[dec.SlotNumber].Command.SameCmd(dec.Command) {
		tempreply.Okay = false
		chatty(2, fmt.Sprintf("\n    Failed in decide:  \n    [%d] already has ------> [%s] ", n.Slot[dec.Slot.HighestN].HighestN, n.Slot[dec.Slot.HighestN].Command.ShowCmd()))
		*reply = tempreply
		return nil
	}
	//quit if already decided
	if dec.Slot.Decided {
		chatty(2, fmt.Sprintf("Failed in decide:  Slot [%d] from [%s] already decided ", dec.Slot.HighestN, dec.Command.Address))
		tempreply.Okay = false
		*reply = tempreply
		return nil
	}
	dec.Slot.Decided = true
	n.Slot[dec.SlotNumber] = dec.Slot

	tempreply.Okay = true
	n.ToApply++
	chatty(1, fmt.Sprintf("[%v] Decide: --> Applying Command={%v}\n", n.ToApply, dec.Command))
	if err := call(n.Address, "Node.RunCommand", dec.Command, &reply); err != nil {

		return err
	}

	if _, okay := n.Acks[strconv.Itoa(dec.Command.Tag)]; okay {
		n.Acks[strconv.Itoa(dec.Command.Tag)] <- reply.Reply
	}
	n.ToApply++
	*reply = tempreply
	n.Mutex.Unlock()

	return nil
}

func (n *Node) Propose(cmd Command, reply *PResponse) error {
	time.Sleep(time.Second * 2)
	n.Mutex.Lock()

	count := 1
	slot := Slot{}
	slot.Decided = false
	slot.Command = cmd
	slot.Promise = cmd.Seq
	slot.Accepted = cmd
	slot.HighestN = 0

	//prepare request to send
	var prequest Prepare
	prequest.Slot = slot
	prequest.Sequence = cmd.Seq
	prequest.SlotNumber = n.ToApply
	accept := Accept{}

	accyes := 0
	accmajority := 0
	preparedone := false
	// PREPARE ROUND
	//blank command in case anyone replies with a higher one.
	mostRecentCommand := PResponse{}
	highestSeq := 0
	n.Mutex.Unlock()
	for { // if prepare round fails find the next highest Sequence Number
		time.Sleep(time.Second * 2)
		// pick first undecided
		for i := 0; i < len(n.Slot); i++ {
			index := i
			value := n.Slot[index]
			chatty(0, fmt.Sprintf("Looking for available slot: [%d] decided --> [%t]", index, value.Decided))
			if !value.Decided {
				//first undecided slot
				slot.HighestN = index
				chatty(1, fmt.Sprintf("Picked Slot: %d\n", prequest.SlotNumber))
				break
			}
		}

		accept.Slot = slot

		chatty(3, fmt.Sprintf("\n     Propose: round %d\n     Slot: %d \n     Sequence: %d\n", count, prequest.SlotNumber, accept.Slot.Promise.Number))
		responses := make(chan PResponse, len(n.Cell))

		for _, c := range n.Cell {
			time.Sleep(time.Second * 2)
			chatty(0, fmt.Sprintf("Running prepare on [%s]\n", c))
			go func(c string, slot Slot, sequence Seq, responses chan PResponse) {
				p := Prepare{
					Slot:       prequest.Slot,
					Sequence:   prequest.Sequence,
					SlotNumber: prequest.SlotNumber,
				}
				reply := PResponse{}
				fmt.Printf("[%v] Propose: starting prepare round with N=[%v]\n", n.ToApply, prequest.Sequence.Number)
				if err := call(c, "Node.Prepare", p, &reply); err != nil {
					fmt.Printf("Error calling function Node.Prepare %v", err)
				}
				// put reponses into the channel
				responses <- reply
			}(c, slot, slot.Promise, responses)
		}
		yes := 0
		no := 0

		for _, c := range n.Cell {
			// pull from the channel response
			response := <-responses
			responseSeq := response.Slot.Promise
			responseCommand := response.Command
			chatty(1, fmt.Sprintf("Response from node.prepare was [%t] Adress [%s]", response.Okay, c))
			//count the replies
			println("CELL", n.Cell)
			println("OK", response.Okay)
			if response.Okay {
				yes++
			} else {
				no++
			}

			// do they have a higher sequence number than me?
			if responseSeq.Number > highestSeq {
				highestSeq = responseSeq.Number
			}
			// does a command have a higher sequence number than the previous champion?
			if responseSeq.Cmp(mostRecentCommand.Promise) > 0 {
				chatty(1, fmt.Sprintf("Higher command recieved from : [ %s ]\nCommand: %s\n", responseCommand.Address, responseCommand.ShowCmd()))
				mostRecentCommand.Command = responseCommand
			}
			majority := len(n.Cell) / 2

			if yes > majority {
				accyes = yes
				accmajority = majority
				preparedone = true
				break
			}
		}
		if preparedone {
			break
		}

	}
	// ACCEPT ROUND
	chatty(1, "Majority votes achieved")
	accept.Command = slot.Command
	accept.Sequence = slot.Promise

	// If one or more of those replicas that voted for you have already ACCEPTED a value pick the highest
	if accyes > accmajority {
		if mostRecentCommand.Command.Tag > 0 && mostRecentCommand.Command.Tag != slot.Command.Tag {
			//println("this is hitting")
			chatty(2, fmt.Sprintf("\n     %s:\n     Already proposed %s", mostRecentCommand.Command.Address, mostRecentCommand.Command.ShowCmd()))
			accept.Command = mostRecentCommand.Command
			accept.Slot.Command = mostRecentCommand.Command
			accept.Sequence = mostRecentCommand.Promise

			call(n.Address, "Node.Accept", accept, &reply)
		} else {
			preparedone = true
		}
		// higher seq because we failed
		chatty(1, fmt.Sprintf("Incrementing sequence in propose: [%d]\n", slot.Promise.Number))
		slot.Promise.Number = highestSeq + 1
		count++
	}
	n.Mutex.Lock()
	slotToWork := accept.Slot
	requestedCmd := accept.Command
	requestedSeq := accept.Sequence
	n.Mutex.Unlock()
	// ask everyone to accept
	responses := make(chan PResponse, len(n.Cell))
	for _, address := range n.Cell {
		go func(address string, slotToWork Slot, sequence Seq, command Command, responses chan PResponse) {
			acc := Accept{
				Slot:     slotToWork,
				Sequence: sequence,
				Command:  command,
			}
			pReply := PResponse{}

			if err := call(address, "Node.Accept", acc, &pReply); err != nil {
				print(err)
				chatty(2, fmt.Sprintf("Connection failure in Accept ---->   [%s]\n", address))
				return
			}
			// Send the response over a channel
			responses <- pReply
		}(address, slotToWork, requestedSeq, requestedCmd, responses)
	}

	// Get responses
	yes := 0
	no := 0
	highestSeqN := 0
	for range n.Cell {
		// pull from the channel response
		tempResponse := <-responses
		//need slot
		responseSeqN := slotToWork
		println("highslotnum", responseSeqN.HighestN)
		if tempResponse.Okay {
			yes++
		} else {
			no++
		}

		// make note of the highest n value that any replica returns to you
		if responseSeqN.Promise.Number > highestSeqN {
			highestSeqN = responseSeqN.Promise.Number
		}
		majority := len(n.Cell) / 2
		println("yes votes", yes)
		println("no votes", no)
		println("MAJORITY", majority)

		if yes > majority {
			break
		}
	}

	majority := len(n.Cell) / 2
	dresponses := make(chan DResponse, len(n.Cell))
	if yes > majority {
		for _, address := range n.Cell {
			go func(address string, slotToWork Slot, command Command, dresponses chan DResponse) {
				dec := Decision{
					Slot:       slotToWork,
					Command:    command,
					SlotNumber: slotToWork.HighestN,
				}
				dReply := DResponse{}
				call(address, "Node.Decide", dec, &dReply)
				dresponses <- dReply
			}(address, slotToWork, requestedCmd, dresponses)

		}
		dno := 0

		for range n.Cell {
			// pull from the channel response
			tempResponse := <-dresponses
			//need slot

			if tempResponse.Okay == false {
				dno++
			}
		}
		if dno > majority {
			var seq2 Seq
			seq2.Number = highestSeqN + 1
			seq2.Address = n.Address

			cmd := Command{
				Command: requestedCmd.Command,
				Seq:     seq2,
			}

			newPReply := PResponse{}

			if err := call(n.Address, "Node.Propose", cmd, &newPReply); err != nil {
				chatty(2, "Failed to connect to myself in Accept calling Propose")
				return err
			}
		}

	}

	return nil
}

func shell(n *Node) {

	// display options
	log.Printf("Starting shell")
	log.Printf("Commands are: get, put, delete, dump, delete, quit and help")
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		// trims leading and trailing whitespace
		line = strings.TrimSpace(line)

		//splits line by spaces into two parts
		parts := strings.SplitN(line, " ", 2)
		if len(parts) > 1 {
			parts[1] = strings.TrimSpace(parts[1])
		}
		if len(parts) == 0 {
			continue
		}
		responseChan := make(chan string, 1)
		switch parts[0] {
		case "get":
			if len(parts) != 2 {
				log.Printf("you must specify a key to get")
				continue
			}
			var cmd Command
			reply := PResponse{}
			cmd.Command = parts[0] + " " + parts[1]
			cmd.Address = n.Address
			cmd.Tag = rand.Intn(100000000)
			var seq Seq
			seq.Address = getLocalAddress()
			seq.Number = 0

			// Create the listen for response channel
			n.Acks[strconv.Itoa(cmd.Tag)] = responseChan

			if err := call(n.Address, "Node.Propose", cmd, &reply); err != nil {
				log.Println(err)
			}
			go func() {
				chatty(2, fmt.Sprintf("DONE: [%s]\n", <-n.Acks[strconv.Itoa(cmd.Tag)]))
			}()

		case "put":
			if len(parts) != 2 {
				log.Printf("you must specify a key value pair to put")
				continue
			}
			var cmd Command
			reply := PResponse{}
			cmd.Command = parts[0] + " " + parts[1]
			cmd.Address = n.Address
			cmd.Tag = rand.Intn(100000000)
			var seq Seq
			seq.Address = getLocalAddress()
			seq.Number = 0
			//responseChan := make(chan string, 1)

			// Create the listen for response channel
			n.Acks[strconv.Itoa(cmd.Tag)] = responseChan

			if err := call(n.Address, "Node.Propose", cmd, &reply); err != nil {
				log.Println(err)
			}

			go func() {
				chatty(2, fmt.Sprintf("DONE: [%s]\n", <-n.Acks[strconv.Itoa(cmd.Tag)]))
			}()

		case "dump":
			var junk Nothing
			var trash Nothing
			if err := call(n.Address, "Node.Dump", &junk, &trash); err != nil {
				log.Fatalf("calling Node.Dump: %v", err)

			}
		case "delete":
			if len(parts) != 2 {
				log.Printf("you must specify a key to delete")
				continue
			}
			var seq Seq
			seq.Address = getLocalAddress()
			seq.Number = 0
			var cmd Command
			reply := PResponse{}
			cmd.Command = parts[0] + " " + parts[1]
			cmd.Address = n.Address
			cmd.Tag = rand.Intn(100000000)
			//responseChan := make(chan string, 1)

			// Create the listen for response channel
			n.Acks[strconv.Itoa(cmd.Tag)] = responseChan

			if err := call(n.Address, "Node.Propose", cmd, &reply); err != nil {
				log.Println(err)
			}

			go func() {
				chatty(2, fmt.Sprintf("DONE: [%s]\n", <-n.Acks[strconv.Itoa(cmd.Tag)]))
			}()

		case "help":
			log.Println("List of commands include:")
			log.Println("get <key>:")
			log.Println("put <key> <value>:")
			log.Println("delete <key>:")
			log.Println("dump:")
			log.Println("help:")
			log.Println("quit:")
		case "":
			log.Println("List of commands include:")
			log.Println("get <key>:")
			log.Println("put <key> <value>:")
			log.Println("delete <key>:")
			log.Println("dump:")
			log.Println("help:")
			log.Println("quit:")
		case "quit":
			os.Exit(0)
		default:
			log.Printf("I only recognize \"get\", \"put\", \"delete\", \"dump\", \"delete\", \"quit\" and \"help\"")
		}

	}
	if err := scanner.Err(); err != nil {
		log.Fatalf("scanner error: %v", err)
	}

}
func printUsage() {
	log.Printf("Usage: %s [address:port] [adress:port] must specify at least two addresses", os.Args[0])
}

func main() {
	var Address = getLocalAddress()
	//process command line arguments
	flag.Parse()

	Address += ":" + flag.Args()[0]
	fmt.Printf("listening on: [%s] \n", Address)
	var Node = createserver(Address, flag.Args()[0:])

	shell(Node)
}
