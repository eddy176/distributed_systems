package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"strconv"
	"strings"
)

func getLocalAddress() string {
	var localaddress string

	ifaces, err := net.Interfaces()
	if err != nil {
		panic("getLocalAddress: failed to find network interfaces")
	}

	// find the first non-loopback interface with an IPv4 address
	for _, elt := range ifaces {
		if elt.Flags&net.FlagLoopback == 0 && elt.Flags&net.FlagUp != 0 {
			addrs, err := elt.Addrs()
			if err != nil {
				panic("getLocalAddress: failed to get addresses for network interface")
			}

			for _, addr := range addrs {
				if ipnet, ok := addr.(*net.IPNet); ok {
					if ip4 := ipnet.IP.To4(); len(ip4) == net.IPv4len {
						localaddress = ip4.String()
						break
					}
				}
			}
		}
	}
	if localaddress == "" {
		panic("localaddress: failed to find non-loopback interface with valid IPv4 address")
	}

	return localaddress
}

//Dump gives all information for a node
func (n *Node) Dump(junk *Nothing, trash *Nothing) error {
	log.Println("  Node: ", "{ ", n.Address, "}")
	log.Print("  Cell {  ")
	for _, c := range n.Cell {
		fmt.Print(c, " ")
	}
	fmt.Println("}")
	fmt.Println("Data: ")
	for key, value := range n.database {
		if value != nil {
			fmt.Printf("   [%s] %s\n", key, value)
		}

	}
	return nil
}

func chatty(level int, line string) {
	CHATTY, _ = strconv.Atoi(line)

	if level >= CHATTY {
		if level == 1 {
			fmt.Println(line)
		} else {
			log.Println(line)
		}
	}
}

func (this Command) SameCmd(that Command) bool {
	return this.Command == that.Command && this.Tag == that.Tag && this.Address == that.Address
}
func (elt Command) ShowCmd() string {
	return fmt.Sprintf("[ %s , %s , %d ]", elt.Address, elt.Command, elt.Tag)
}

//Cmp compares values in seq
func (this Seq) Cmp(that Seq) int {
	myNum := this.Number
	otherNum := that.Number
	myAddress := this.Address
	otherAddress := that.Address

	//-1 less than, 0 equal, 1 greater than

	if myNum == otherNum {
		if otherAddress != "" {
			if myAddress > otherAddress {
				return 1
			}
		}
		if myAddress < otherAddress {
			return -1
		}
		return 0
	}
	if myNum > otherNum {

		return 1
	}
	if myNum < otherNum {
		return -1
	}
	return 0
}

func (n *Node) RunCommand(com Command, reply *DResponse) error {

	commandType := strings.Fields(com.Command)[0]
	commandKey := strings.Fields(com.Command)[1]
	switch {
	case commandType == "get":
		for key, value := range n.database {
			if key == commandKey {
				reply.Key = commandKey
				reply.Value = value
				reply.Address = n.Address
				reply.Requested = "get"
				justString := strings.Join(value, "")
				reply.Reply = "Key: " + commandKey + " " + "Value: " + justString
			} else {
				reply.Reply = "Does not Exist!"
			}

		}
	case commandType == "put":
		k := strings.Fields(com.Command)[1]
		v := strings.Fields(com.Command)[2:]
		n.database[k] = v
		reply.Requested = "put"
		val := strings.Join(v, "")
		reply.Reply = "Put ---> " + val + " ---> " + k
	case commandType == "delete":
		k := strings.Fields(com.Command)[1]
		for key, value := range n.database {
			if key == k {
				reply.Key = k
				reply.Value = value
				reply.Address = n.Address
				reply.Requested = "delete"
				val := strings.Join(value, "")
				reply.Reply = "Deleted: ---> " + k + " " + val
				delete(n.database, k)
				return nil
			} else {
				reply.Reply = "Does not Exist!"
			}
		}
	default:
		chatty(2, "Command not recognized\n")
	}
	return nil
}

func createserver(address string, cell []string) *Node {
	node := new(Node)
	node.Address = address
	node.Slot = make([]Slot, 100)
	node.database = make(map[string][]string)
	node.Acks = make(map[string]chan string)
	node.ToApply = 0

	localaddress := getLocalAddress()
	for _, addr := range cell {
		node.Cell = append(node.Cell, localaddress+":"+addr)
	}
	rpc.Register(node)
	rpc.HandleHTTP()

	l, err := net.Listen("tcp", node.Address)
	if err != nil {
		log.Fatal("listening error: ", err)
	}

	go http.Serve(l, nil)
	return node
}
func call(address string, method string, request interface{}, response interface{}) error {
	client, err := rpc.DialHTTP("tcp", address)
	if err != nil {
		log.Printf("rpc.DialHTTP: %v", err)
		return err
	}
	defer client.Close()
	if (method) != "quit" {
		if err = client.Call(method, request, response); err != nil {
			log.Printf("client.Call %s: %v", method, err)
			return err
		}
	}
	return nil
}
