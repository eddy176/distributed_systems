package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	address := ":8080"
	tempdir := "/tmp"

	fmt.Println("test")

	log.Printf("starting http server at %s", address)
	http.Handle("/data", http.FileServer(http.Dir(tempdir)))

	//http.Handle("/data/", http.FileServer(http.Dir(tempdir)))
	http.Handle("/data/", http.StripPrefix("/data", http.FileServer(http.Dir(tempdir))))
	if err := http.ListenAndServe(address, nil); err != nil {
		log.Fatalf("Error in HTTP server for %s: %v", address, err)
	}
	/*
		if err := http.ListenAndServe(address, nil); err != nil {
			log.Printf("Error in HTTP server for %s: %v", address, err)
		}
	*/

}
