package main

import (
	"crypto/sha1"
	"log"
	"math/big"
	"net"
	"strconv"
)

var two = big.NewInt(2)
var hashMod = new(big.Int).Exp(big.NewInt(2), big.NewInt(keySize), nil)

//print out hash codes in a format that makes it easy to visually compare two hash values
//hex := fmt.Sprintf("%040x", hashString(value))
//s := hex[:8] + ".. (" + string(value) + ")"

//get a sha1 hash value
func hashString(elt string) *big.Int {
	hasher := sha1.New()
	hasher.Write([]byte(elt))

	return new(big.Int).SetBytes(hasher.Sum(nil))

}

//computes the address of a position across the ring that should be pointed to by the given finger table entry
func jump(address string, fingerentry int) *big.Int {
	n := hashString(address)
	fingerentryminus1 := big.NewInt(int64(fingerentry) - 1)
	jump := new(big.Int).Exp(two, fingerentryminus1, nil)
	sum := new(big.Int).Add(n, jump)

	return new(big.Int).Mod(sum, hashMod)
}

//returns true if elt is between start and end on the ring, accounting for the boundary where the ring loops back on itself.
//If inclusive is true, it tests if elt is in (start,end], otherwise it tests for (start,end).
func between(start, elt, end *big.Int, inclusive bool) bool {
	if end.Cmp(start) > 0 {
		return (start.Cmp(elt) < 0 && elt.Cmp(end) < 0) || (inclusive && elt.Cmp(end) == 0)
	}
	return start.Cmp(elt) < 0 || elt.Cmp(end) < 0 || (inclusive && elt.Cmp(end) == 0)
}

//finds the first address that is not a loopback device, so it should be one that an outside machine can connect to
func getLocalAddress() string {
	var localaddress string

	ifaces, err := net.Interfaces()
	if err != nil {
		panic("init: failed to find network interfaces")
	}

	// find the first non-loopback interface with an IP address
	for _, elt := range ifaces {
		if elt.Flags&net.FlagLoopback == 0 && elt.Flags&net.FlagUp != 0 {
			addrs, err := elt.Addrs()
			if err != nil {
				panic("init: failed to get addresses for network interface")
			}

			for _, addr := range addrs {
				if ipnet, ok := addr.(*net.IPNet); ok {
					if ip4 := ipnet.IP.To4(); len(ip4) == net.IPv4len {
						localaddress = ip4.String()
						break
					}
				}
			}
		}
	}
	if localaddress == "" {
		panic("init: failed to find non-loopback interface with valid address on this node")
	}

	return localaddress
}

//ClosestProceedingNode gets the next predecessor
func (s Server) ClosestProceedingNode(address string, reply *string) error {
	log.Print("next")

	id := hashString(address)
	i := keySize
	x := strconv.Itoa(node.Port)
	for i > 1 {
		log.Print("next and hiitting")
		if inti, err := strconv.Atoi(node.Fingertable[i]); err != nil {
			log.Print("inti ", inti)
			log.Print("fingertable ", node.Fingertable[i])
			log.Print("working?")
			var n = int64(inti)
			println("error converting ", err)

			bigi := big.NewInt(n)
			log.Print("working now?")
			if between(hashString(x), bigi, id, false) {
				log.Print("how about now???")
				*reply = node.Fingertable[i]
			}
			i--
		}
	}
	log.Print("next adn still hitting")
	*reply = x
	return nil
}

//FindSuccessor finds successor
func (s Server) FindSuccessor(id string, reply *string) error {
	log.Print("finding")
	if between(hashString(strconv.Itoa(node.Port)), hashString(id), hashString(node.Successor), true) {
		log.Print("finding and hitting")
		*reply = node.Successor
		return nil
	}
	log.Print("finding and still hitting")
	var messages *string
	s.ClosestProceedingNode(id, messages)
	log.Print("done looking")
	s.FindSuccessor(id, reply)
	log.Print("not hitting")
	return nil

}

func (s Server) stabilize() error {
	//doesnt stabalize when a node leaves. fix when finger table is done
	address := defaultHost + ":" + strconv.Itoa(defaultPort)
	var messages2 string
	var messages string
	s.FindSuccessor(address, &messages2)
	s.ClosestProceedingNode(messages2, &messages)
	p := strconv.Itoa(node.Port)
	if messages != "" && between(hashString(p), hashString(messages), hashString(node.Successor), false) {
		log.Print("stabalize: successors list changed")
		node.Successor = messages
	}
	var messages3 string
	s.Notify(messages2, &messages3)

	return nil
}

//Notify something
func (s Server) Notify(n string, reply *string) error {
	n1 := strconv.Itoa(node.Port)
	if node.Predecessor == "" || between(hashString(node.Predecessor), hashString(n), hashString(n1), false) {
		node.Predecessor = n
		log.Print("notify: predecessor set to ", node.Predecessor)
		*reply = "Yes, you are now my predecessor, from " + n1
	}
	return nil
}

//FixFingers updates fingertable
func (s Server) FixFingers(request, _ *Nothing) error {
	node.Fingertable[1] = node.Successor
	// Determine the offset
	// Find the successor
	// Update the finger table
	node.Next = node.Next + 1
	if node.Next > keySize {
		node.Next = 1
	}
	reply := ""
	address := defaultHost + ":" + strconv.Itoa(node.Port)
	n := jump(address, node.Next)
	n1 := n.String()
	s.FindSuccessor(n1, &reply)

	for node.Next+1 < keySize && between(hashString(strconv.Itoa(node.Port)), jump(address, node.Next+1), hashString(reply), false) {
		node.Next++
		node.Fingertable[node.Next] = reply
	}
	node.Fingertable[node.Next] = reply
	return nil
}
