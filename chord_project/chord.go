package main

import (
	"bufio"
	"crypto/sha1"
	"flag"
	"log"
	"net/rpc"
	"os"
	"strconv"
	"strings"
)

const (
	defaultHost = "127.0.0.1"
	defaultPort = 3410
	keySize     = sha1.Size * 8
)

//Key for node struct
type Key string

//Node info
type Node struct {
	MyAddress   string
	Port        int
	Predecessor string
	Successor   string
	Successors  []string
	Running     bool
	Fingertable [161]string
	Next        int
}

var node = &Node{
	Port:    3410,
	Running: false,
}

//Bucket for key value pair
var Bucket = make(map[string]string)

//Feed messages owned by actor
type Feed struct {
	Messages []string
}
type handler func(*Feed)

//Nothing struct
type Nothing struct{}

func client(address string) {

	var junk Nothing
	if err := call(address, "Server.Post", "This is cool", &junk); err != nil {
		log.Fatalf("client.Call; %v", err)
	}
	if err := call(address, "Server.Post", "So super cool", &junk); err != nil {
		log.Fatalf("client.Call; %v", err)
	}

	var lst []string
	if err := call(address, "Server.Get", 5, &lst); err != nil {
		log.Fatalf("client.Call Get; %v", err)
	}

	for _, elt := range lst {
		log.Println(elt)
	}
}

func call(address string, method string, request interface{}, response interface{}) error {

	client, err := rpc.DialHTTP("tcp", address)
	if err != nil {
		log.Printf("rpc.DialHTTP: %v", err)
		return err
	}
	defer client.Close()
	if (method) != "quit" {
		if err = client.Call(method, request, response); err != nil {
			log.Printf("client.Call %s: %v", method, err)
			return err
		}
	}
	return nil
}

func shell(address string) {
	// display options
	log.Printf("Starting shell")
	log.Printf("Commands are: get, put, delete, post, port, create, join, dump, delete, quit and help")
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		// trims leading and trailing whitespace
		line = strings.TrimSpace(line)

		//splits line by spaces into two parts
		parts := strings.SplitN(line, " ", 2)
		if len(parts) > 1 {
			parts[1] = strings.TrimSpace(parts[1])
		}
		if len(parts) == 0 {
			continue
		}

		switch parts[0] {
		case "get":

			var messages string
			if err := call(address, "Server.Get", parts[1], &messages); err != nil {
				log.Fatalf("calling Server.Get: %v", err)
			}
			if messages == "" {
				print("Value not found!")
			}
			println(string(messages))
		case "put":
			if len(parts) != 2 {
				log.Printf("you must specify a key value pair to put")
				continue
			}

			var messages string
			println("PARTS ", parts[1])
			if err := call(address, "Server.PutRequest", parts[1], &messages); err != nil {
				log.Fatalf("calling Server.PutRequest: %v", err)

			}
			log.Print(messages)
		case "post":
			if len(parts) != 2 {
				log.Printf("you must specify a message to post")
				continue
			}
			var junk Nothing
			if err := call(address, "Server.Post", parts[1], &junk); err != nil {
				log.Fatalf("calling Server.Post: %v", err)

			}
		case "port":
			if node.Running == false {
				x := defaultPort
				if len(parts) == 2 {
					var err error
					if x, err = strconv.Atoi(parts[1]); err != nil {
						log.Fatalf("setting port: %v", err)
					}
				}
				var messages []string
				if err := call(address, "Server.Port", x, &messages); err != nil {
					log.Fatalf("calling Server.Port: %v", err)
				}
				node.Port = x
				for _, elt := range messages {
					log.Println(elt)

				}
				log.Println("listening on port: ", node.Port)
			} else {
				println("node already running!")
			}

		case "create":
			if node.Running == false {
				var messages []string
				if len(parts) == 2 {
					node.Running = true
					if err := call(address, "Server.Create", parts[1], &messages); err != nil {
						log.Fatalf("calling Server.Create: %v", err)
					}
				} else {
					node.Running = true
					if err := call(address, "Server.Create", strconv.Itoa(node.Port), &messages); err != nil {
						log.Fatalf("calling Server.Create: %v", err)
					}
				}
			} else {
				println("node already running!")
			}
		case "dump":
			var messages []string
			if err := call(address, "Server.Dump", parts[0], &messages); err != nil {
				log.Fatalf("calling Server.Dump: %v", err)
			}
		case "join":
			if node.Running == false {

				if len(parts) != 2 {
					log.Printf("you must specify a port to join")
					continue
				}
				var messages []string
				node.Running = true
				if err := call(address, "Server.Join", parts[1], &messages); err != nil {
					log.Fatalf("calling Server.Join: %v", err)

				}
			} else {
				println("node already running!")
			}
		case "delete":
			if len(parts) != 2 {
				log.Printf("you must specify a key to delete")
				continue
			}
			var messages string
			if err := call(address, "Server.Delete", parts[1], &messages); err != nil {
				log.Fatalf("calling Server.Delete: %v", err)

			}
		case "help":
			log.Println("List of commands include:")
			log.Println("create:")
			log.Println("join <address>:")
			log.Println("quit:")
			log.Println("get <key>:")
			log.Println("delete <key>:")
			log.Println("dump:")
			log.Println("help:")
		case "":
			log.Println("List of commands include:")
			log.Println("create:")
			log.Println("join <address>:")
			log.Println("quit:")
			log.Println("get <key>:")
			log.Println("delete <key>:")
			log.Println("dump:")
			log.Println("help:")
		case "quit":
			var junk Nothing
			call(address, "Server.Quit", parts[0], &junk)
			os.Exit(0)
		default:
			log.Printf("I only recognize \"get\", \"put\", \"delete\", \"post\", \"port\", \"create\", \"join\", \"dump\", \"delete\", \"quit\" and \"help\"")
		}

	}
	if err := scanner.Err(); err != nil {
		log.Fatalf("scanner error: %v", err)
	}

}
func printUsage() {
	log.Printf("Usage: %s [-server or -client] [adress]", os.Args[0])
	flag.PrintDefaults()
	os.Exit(1)
}

func main() {
	var isServer bool
	var isClient bool
	var address string
	flag.BoolVar(&isServer, "server", false, "start as my server")
	flag.BoolVar(&isClient, "client", false, "start as my client")
	flag.Parse()

	if isServer && isClient {
		log.Fatalf("can't be both client and server")
	}
	if !isServer && !isClient {
		printUsage()
	}

	switch flag.NArg() {
	case 0:

		if isClient {
			address = defaultHost + ":" + strconv.Itoa(defaultPort)
			log.Println("Address: " + address)
		} else {
			address = ":" + strconv.Itoa(defaultPort)
		}

	case 1:

		// user  specified the address
		address = flag.Arg(0)

	default:
		printUsage()
	}

	if isClient {
		shell(address)
	} else {
		server(address)
	}
}
