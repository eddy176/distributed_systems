package main

import (
	"log"
	"math"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"strconv"
	"strings"
	"time"
)

//Server channel restricted to writes only
type Server chan<- handler

//Post messages to server handed to actor
func (s Server) Post(msg string, reply *Nothing) error {
	finished := make(chan struct{})
	s <- func(f *Feed) {
		f.Messages = append(f.Messages, msg)
		finished <- struct{}{}
	}
	//block and wait until value comes across finish channel
	<-finished
	return nil
}

//Dump gives all information for a node
func (s Server) Dump(request string, reply *[]string) error {
	println(len(Bucket))
	log.Print("  Data Items:  ")
	for k, v := range Bucket {
		log.Print("key ", k, " -> ", "value ", v)
	}

	return nil
}

//Put stores key value pair in a node
func (s Server) Put(args []string, reply *string) error {
	finished := make(chan struct{})

	s <- func(f *Feed) {

		key := string(args[0])
		println("key", key)
		value := strings.TrimSpace(string(args[1]))
		Bucket[key] = value
		println(value)
		println(Bucket[key])
		if Bucket[key] == value {
			*reply = value + " Added to " + defaultHost + ":" + strconv.Itoa(node.Port) + "'s bucket"

		} else {
			*reply = string("Could not add data")
		}
		finished <- struct{}{}
	}
	<-finished
	return nil
}

//PutRequest finds address for user to call put
func (s Server) PutRequest(line string, response *string) error {
	reply := ""
	list := strings.Fields(line)
	key := list[0]
	value := list[1]
	ip := ""
	log.Println("this is hitting")
	s.FindSuccessor(key, &ip)
	log.Println("this is hitting")
	address := defaultHost + ":" + strconv.Itoa(node.Port)
	args := []string{key, value, address}
	if err := s.Put(args, &reply); err != nil {
		log.Println(err)
		return nil
	}

	log.Println("this is hitting")
	response = &reply
	return nil
}

//Delete removes key value from bucket map
func (s Server) Delete(args []string, reply *string) error {
	finished := make(chan struct{})

	s <- func(f *Feed) {

		key := string(args[0])
		value := strings.TrimSpace(string(args[1]))

		if _, present := Bucket[key]; present {
			*reply = key + " deleted from " + defaultHost + ":" + strconv.Itoa(node.Port) + "'s bucket"
			log.Println(key+"was deleted from your bucket", value)
			delete(Bucket, key)
		} else {
			*reply = "Could not delete data"
		}

		finished <- struct{}{}
	}
	<-finished
	return nil
}

func (s Server) deleteRequest(line string) error {
	reply := ""
	list := strings.Fields(line)
	key := list[0]
	address := defaultHost + ":" + strconv.Itoa(node.Port)
	args := []string{key, address}
	if err := s.Delete(args, &reply); err != nil {
		log.Println(err)
		return nil
	}
	log.Println(reply)
	return nil
}

//Get messages from server
//finish get dump delete quit
func (s Server) Get(key string, reply *string) error {
	finished := make(chan struct{})
	s <- func(f *Feed) {

		*reply = Bucket[key]
		finished <- struct{}{}

	}
	//block and wait until value comes across finish channel
	<-finished
	return nil
}

func (s Server) getRequest(line string) error {
	reply := ""
	list := strings.Fields(line)
	key := list[0]
	ip := string(node.Port)
	s.FindSuccessor(key, &ip)
	s.Get(key, &reply)
	log.Println(reply)
	return nil
}

//Port sets the port a node should listen on 3410 by def
func (s Server) Port(request int, reply *[]string) error {
	var x float64
	x = math.Pow(2, 16)
	if request > 0 && request < int(x) {
		node.Port = request
	} else {
		log.Println("that is not a valid port")
	}
	log.Println("listening on port: ", node.Port)

	return nil
}

//Create makes a new chord ring unless a ring is already running it returns nil
func (s Server) Create(adress string, reply *[]string) error {
	finished := make(chan struct{})
	s <- func(f *Feed) {
		log.Print("New Node: creating new ring")
		node.Predecessor = ""
		node.Successor = strconv.Itoa(node.Port)

		log.Print("Starting to listen on ", adress)
		finished <- struct{}{}
	}
	go func() {
		for {
			s.stabilize()

			time.Sleep(time.Second)

		}
	}()
	<-finished
	return nil
}

//Join adds node to ring
func (s Server) Join(address string, reply *[]string) error {
	finished := make(chan struct{})
	s <- func(f *Feed) {
		log.Print("Joining ring")
		node.Predecessor = ""
		node.Successor = address

		log.Print("Listening on ", address)
		finished <- struct{}{}
	}
	<-finished
	return nil

}

//Help gives list of available commands
func (s Server) Help(request string, reply *[]string) error {
	finished := make(chan struct{})
	s <- func(f *Feed) {

		*reply = make([]string, 8)
		copy(*reply, f.Messages[len(f.Messages)-8:])
		finished <- struct{}{}
	}
	<-finished
	return nil
}

//Quit quits
func (s Server) Quit(line string, reply *Nothing) error {
	log.Println("quitting")
	os.Exit(0)
	return nil
}

//StartActor creates channel uses background go routine to grab element from channel which are funcs then runs one at a time
func StartActor() Server {
	ch := make(chan handler)
	state := new(Feed)
	go func() {
		for f := range ch {
			f(state)
		}
	}()
	return ch
}

func server(address string) {
	actor := StartActor()
	rpc.Register(actor)
	rpc.HandleHTTP()
	l, e := net.Listen("tcp", address)
	if e != nil {
		log.Fatal("listen error:", e)
	}
	if err := http.Serve(l, nil); err != nil {
		log.Fatalf("http.Serve: %v", err)
	}
}
